<?php
/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 2019-11-17
 * Time: 15:02
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class InvirtusController extends AbstractController
{

    /**
     * Show location history
     * @Route("/view", name="view")
     */
    public function view()
    {
        return $this->render('view.html.twig', ['id' => 1]);
    }

    /**
     * Retrieve location huistory for an equipment
     * @param int $id
     * @Route("/locations/{id}", name="locations")
     * @return JsonResponse
     */
    public function getLocations(int $id) {

        //Read json file
        $jsonLocations = file_get_contents(__DIR__.'/../resources/locations.json');

        //Decode json
        $locations = json_decode($jsonLocations);

        //Access to the good device data
        $deviceLocationHistory = $locations->$id;

        return new JsonResponse($deviceLocationHistory, 200);

    }
}